from fastapi import FastAPI, HTTPException, WebSocket, WebSocketDisconnect
from pydantic import BaseModel
from enum import Enum
import sqlite3
from random import choice
import asyncio
from typing import Optional


DATABASE_NAME = "images.db"


class ApiTags(Enum):
    images = "Images"
    tags = "Tags"
    rectangles = "Rectangles"


class SocketGroup:
    def __init__(self):
        self.sockets: [WebSocket] = []

    async def new_socket(self, socket: WebSocket):
        await socket.accept()
        self.sockets.append(socket)

    async def keep_alive(self, socket: WebSocket):
        try:
            while True:
                await socket.receive_text()
        except WebSocketDisconnect:
            self.sockets.remove(socket)

    async def broadcast(self, msg: dict):
        for socket in self.sockets:
            await socket.send_json(msg)

    async def broadcast_image_create(self, id: int):
        await self.broadcast({"tag": "imageCreate", "id": id})

    async def broadcast_rectangle_create(self, id: int, image_id: int):
        await self.broadcast({"tag": "rectangleCreate", "id": id, "imageId": image_id})

    async def broadcast_rectangle_delete(self, id: int, image_id: int):
        await self.broadcast({"tag": "rectangleDelete", "id": id, "imageId": image_id})


socket_group = SocketGroup()

app = FastAPI()
conn = sqlite3.connect("images.db")


class Id(BaseModel):
    id: int


class Name(BaseModel):
    name: str


class Tag(Name, Id):
    imageCount: int


class RectangleConfig(BaseModel):
    x: int
    y: int
    width: int
    height: int
    color: str


class Rectangle(RectangleConfig, Id):
    image: int


class Image(Name, Id):
    tags: list[Tag]
    rectangles: list[Rectangle]


class ImageTagRelation(Id):
    image: int
    tag: int


conn.execute("PRAGMA foreign_keys = ON")
conn.execute(
    """
    CREATE TABLE IF NOT EXISTS Tag (
        id INTEGER PRIMARY KEY,
        name TEXT UNIQUE NOT NULL
    )
    """
)
conn.execute(
    """
    CREATE TABLE IF NOT EXISTS Image (
        id INTEGER PRIMARY KEY,
        name TEXT NOT NULL
    )
    """
)
conn.execute(
    """
    CREATE TABLE IF NOT EXISTS Rectangle (
        id INTEGER PRIMARY KEY,
        x INTEGER NOT NULL,
        y INTEGER NOT NULL,
        width INTEGER NOT NULL,
        height INTEGER NOT NULL,
        color TEXT NOT NULL,
        image INTEGER NULL,
        FOREIGN KEY (image) REFERENCES Image ON DELETE CASCADE
    )
    """
)
conn.execute(
    """
    CREATE TABLE IF NOT EXISTS ImageTagRelation(
        id INTEGER PRIMARY KEY,
        image INTEGER NOT NULL,
        tag INTEGER NOT NULL,
        FOREIGN KEY (image) REFERENCES Image ON DELETE CASCADE,
        FOREIGN KEY (tag) REFERENCES Tag ON DELETE CASCADE,
        UNIQUE (image, tag)
    )
    """
)


def fetch_image_tags(image_id: int) -> list[Tag]:
    query = conn.execute(
        """
        SELECT Tag.id, Tag.name, count(R2.image)
        FROM ImageTagRelation R1
            LEFT JOIN Tag ON R1.tag = Tag.id
            LEFT JOIN ImageTagRelation R2 ON R2.tag = Tag.id
        WHERE R1.image = ?
        GROUP BY Tag.id
        """,
        (image_id,),
    )
    return [
        Tag(id=id, name=name, imageCount=image_count) for id, name, image_count in query
    ]


def fetch_image_rectangles(image_id: int) -> list[Rectangle]:
    query = conn.execute(
        """
        SELECT id, x, y, width, height, color
        FROM Rectangle
        WHERE image = ?
        """,
        (image_id,),
    )
    return [
        Rectangle(
            id=id, x=x, y=y, width=width, height=height, color=color, image=image_id
        )
        for id, x, y, width, height, color in query
    ]


@app.get("/tags", tags=[ApiTags.tags])
async def get_tags() -> list[Tag]:
    """
    Return all tags.
    """

    query = conn.execute(
        """
        SELECT Tag.id, Tag.name, count(R.image)
        FROM Tag
            LEFT JOIN ImageTagRelation R ON R.tag = Tag.id
        GROUP BY Tag.id
        """
    )
    return [
        Tag(id=id, name=name, imageCount=image_count) for id, name, image_count in query
    ]


@app.get("/images/ids", tags=[ApiTags.images])
async def image_count() -> list[int]:
    """
    Get IDs of all images.
    """
    query = conn.execute(
        """
        SELECT id
        FROM Image
        """
    )

    return [i for (i,) in query.fetchall()]


@app.get("/images", tags=[ApiTags.images])
async def get_images() -> list[Image]:
    """
    Return all images and their tags.
    """
    query = conn.execute(
        """
        SELECT Image.id, Image.name
        FROM Image
        """
    )

    return [
        Image(
            id=id,
            name=name,
            tags=fetch_image_tags(id),
            rectangles=fetch_image_rectangles(id),
        )
        for id, name in query
    ]


@app.get("/images/{tag}", tags=[ApiTags.images])
async def get_images_by_tag(tag: int) -> list[Image]:
    """
    Return all images tagged with the provided tag.
    """
    query = conn.execute(
        """
        SELECT Image.id, Image.name
        FROM Image
            LEFT JOIN ImageTagRelation R ON R.image = Image.id
        WHERE R.tag = ?
        """,
        (tag,),
    )
    return [Image(id=id, name=name, tags=fetch_image_tags(id)) for id, name in query]


@app.post("/images/create", tags=[ApiTags.images])
async def create_image(image: Name) -> Id:
    """
    Create image with provided name.
    """
    with conn:
        query = conn.execute(
            """
            INSERT
            INTO Image
            VALUES (null, ?)
            """,
            (image.name,),
        )
    if query.rowcount == 0:
        raise HTTPException(404, "Could not create image.")

    image_id = query.lastrowid

    await socket_group.broadcast_image_create(image_id)

    return Id(id=image_id)


@app.delete("/images/del", tags=[ApiTags.images])
async def delete_image(image: Id) -> str:
    """
    Delete image with provided id.
    """
    with conn:
        query = conn.execute(
            """
            DELETE
            FROM Image
            WHERE Image.id = ?
            """,
            (image.id,),
        )
    if query.rowcount == 0:
        raise HTTPException(404, "Image not found.")

    return "Deleted image."


@app.get("/image/{id}", tags=[ApiTags.images])
async def get_image(id: int, stumble: Optional[bool] = None) -> Image:
    """
    Get image by id.
    """
    query = conn.execute(
        """
        SELECT Image.name
        FROM Image
        WHERE image.id = ?
        """,
        (id,),
    )
    row = query.fetchone()
    if row is None:
        raise HTTPException(404, "Image not found.")

    if stumble:
        roll = choice(["fine", "error", "wait"])
        if roll == "error":
            raise HTTPException(418, "Slipped on a banana peel :/")
        elif roll == "wait":
            await asyncio.sleep(5)

    return Image(
        id=id,
        name=row[0],
        tags=fetch_image_tags(id),
        rectangles=fetch_image_rectangles(id),
    )


@app.post("/image/{image_id}/rectangle", tags=[ApiTags.rectangles])
async def create_rectangle(image_id: int, rect: RectangleConfig) -> Id:
    """
    Add rectangle to image.
    """
    failed = False

    try:
        with conn:
            query = conn.execute(
                """
                INSERT
                INTO Rectangle
                VALUES (null, ?, ?, ?, ?, ?, ?)
                """,
                (rect.x, rect.y, rect.width, rect.height, rect.color, image_id),
            )
    except sqlite3.IntegrityError:
        # This happens when the foreign key constraint is broken.
        failed = True

    if failed or query.rowcount == 0:
        raise HTTPException(404, "Image not found.")

    rect_id = query.lastrowid

    await socket_group.broadcast_rectangle_create(rect_id, image_id)

    return Id(id=rect_id)


@app.get("/image/{image_id}/rectangle/{rect_id}", tags=[ApiTags.rectangles])
async def get_rectangle(image_id: int, rect_id: int) -> Rectangle:
    """
    Get rectangle from image.
    """
    query = conn.execute(
        """
        SELECT x, y, width, height, color
        FROM Rectangle
        WHERE id = ? AND image = ?
        """,
        (
            rect_id,
            image_id,
        ),
    )
    row = query.fetchone()
    if row is None:
        raise HTTPException(404, "Rectangle not found.")
    x, y, width, height, color = row

    return Rectangle(
        id=rect_id, x=x, y=y, width=width, height=height, color=color, image=image_id
    )


@app.delete("/image/{image_id}/rectangle", tags=[ApiTags.rectangles])
async def delete_rectangle(image_id: int, rect: Id) -> str:
    """
    Delete rectangle from image.
    """
    with conn:
        query = conn.execute(
            """
            DELETE
            FROM Rectangle
            WHERE id = ? AND image = ?
            """,
            (
                rect.id,
                image_id,
            ),
        )
    if query.rowcount == 0:
        raise HTTPException(404, "Rectangle not found.")

    await socket_group.broadcast_rectangle_delete(rect.id, image_id)

    return "Deleted rectangle."


@app.websocket("/ws")
async def websocket_endpoint(socket: WebSocket):
    await socket_group.new_socket(socket)
    await socket_group.keep_alive(socket)
