# whiteboard fastapi server

Assignment for [web applications mimuw course](https://kciebiera.github.io/www-2324/).

You can find the frontend [here](https://gitlab.com/barteloniu/awww8).

# how to run

```bash
pip install -r requirements.txt
uvicorn main:app --reload --port 9090
```
